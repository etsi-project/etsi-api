import multer from 'multer';
import { imageFilter } from '../../helpers'
import express from 'express'
import path, { dirname } from 'path'

const router = express.Router();
const isDev = process.env.NODE_ENV !== "production"

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, isDev ? path.resolve(__dirname, './uploads') : '/var/www/static.etsi.ge/uploads/products');
  },

  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

let upload = multer({ storage: storage, fileFilter: imageFilter }).array('product-item', 10);

router.post('/products', upload, (req, res) => {
  res.send(req.files.map(item => {
    return item.path.replace("/var/www/", "https://")
  }))
})


export default router