import express from 'express'
import models from '../../database/models'
import { query, body, validationResult } from 'express-validator'
import { jwtMiddleware } from '../../middlewares'

const router = express.Router();


router.get('/:id',
  [
    query('filters.with_shop')
      .optional().
      isBoolean().
      toBoolean().
      withMessage('value for \'with_shop\' must be boolean type'),
  ],
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const id = req.params.id;
    const { filters } = req.query;

    const include = [
      {
        model: models.ProductImage,
        as: 'images',
      }
    ];

    if (filters) {
      if (filters.with_shop) {
        include.push({
          model: models.Shop,
          as: 'shop',
        });
      }
    }

    const data = await models.Product.findOne({
      where: {
        id: id,
      },
      include,
    });


    res.status(200).send({ data });
    res.end();
  });



export default router