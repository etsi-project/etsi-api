import express from 'express'
import jwt from "jsonwebtoken"

const router = express.Router();

const SECRET_KEY = "rapnika"
const EXPIRY_SECONDS = 300

const users = {
  user1: "password1",
  user2: "password2",
}

router.post('/token', (req, res, next) => {
  const user = {
    username: req.body.username,
    password: req.body.password
  }

  if (!user.username || !user.password || users[user.username] !== user.password) {
    return res.status(401).end()
  }

  const token = jwt.sign({ username: user.username }, SECRET_KEY, {
    algorithm: "HS256",
    expiresIn: EXPIRY_SECONDS,
  })


  res.status(200).send({
    data: {
      token,
      maxAge: EXPIRY_SECONDS * 1000
    }
  })

  res.end()
})

router.post('/refresh', (req, res, next) => {
  let authHeader = req.headers.authorization
  if (authHeader.startsWith("Bearer ")) {
    let token = authHeader.substring(7, authHeader.length);
    var payload
    try {
      payload = jwt.verify(token, SECRET_KEY)
      const nowUnixSeconds = Math.round(Number(new Date()) / 1000)

      if (payload.exp - nowUnixSeconds > 30) {
        return res.status(400).end()
      }

      const newToken = jwt.sign({ username: payload.username }, SECRET_KEY, {
        algorithm: "HS256",
        expiresIn: EXPIRY_SECONDS,
      })

      res.status(200).send({
        data: {
          token,
          maxAge: EXPIRY_SECONDS * 1000
        }
      })

    } catch (e) {
      if (e instanceof jwt.JsonWebTokenError) {
        return res.status(401).end()
      }
      return res.status(400).end()
    }
  } else {
    return res.status(401).end()
  }
})


export default router