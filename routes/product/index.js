import express from 'express'
import models from '../../database/models'
import { query, body, validationResult } from 'express-validator'
import { jwtMiddleware } from '../../middlewares'
import sequelize from 'sequelize'

const router = express.Router();

router.post('/create',
  [
    jwtMiddleware,
    body('name')
      .isString().isLength({ min: 3, max: 20 }).withMessage('string length should be between 3-20 characters'),
    body('description')
      .isString().withMessage('string value for \'description\' is required'),
    body('price')
      .isInt().toInt().withMessage('integer value for \'price\' is required'),
    body('categoryId')
      .isInt().toInt().withMessage('integer value for \'categoryId\' is required'),
    body('options')
      .optional()
  ],
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    try {
      let {
        name,
        description,
        images,
        categoryId,
        price,
        options
      } = req.body
      const t = await models.sequelize.transaction();

      try {

        let userShop = await models.Shop.findOne({
          where: {
            userId: req.user.id
          }
        })

        const product = await models.Product.create({
          name,
          description,
          categoryId,
          shopId: userShop.id,
          profileImage: 'profileimageurl',
          price,
          createdAt: new Date(),
          updatedAt: new Date()
        }, { transaction: t });

        let imagesToInsert = images.map(item => {
          return {
            productId: product.id,
            imageUrl: item,
            createdAt: new Date(),
            updatedAt: new Date()
          }
        })

        let optionsToInsert = []
        if (options) {
          optionsToInsert = options.map(item => {
            return {
              productId: product.id,
              configName: item[0],
              configValue: item[1],
              configSKU: item[2]
            }
          })
        }


        await models.ProductImage.bulkCreate(imagesToInsert, { transaction: t });
        await models.ProductOption.bulkCreate(optionsToInsert, { transaction: t });

        await t.commit();

      } catch (error) {
        await t.rollback();
        return res.send({ data: error, message: "Server error" })
      }

      res.status(200).send({ success: true });
    } catch (e) {
      res.send({ data: e })
    }

    res.end();
  });


export default router