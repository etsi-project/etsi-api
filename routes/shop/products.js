import express from 'express'
import models from '../../database/models'
import { query, body, validationResult } from 'express-validator'
import { jwtMiddleware } from '../../middlewares'
import { Op, literal } from 'sequelize'

const router = express.Router();

router.get('/:shopId/products',
  [
    query('page')
      .isInt()
      .toInt()
      .withMessage('integer value for \'page\' is required'),
    query('perPage')
      .toInt()
      .isInt().withMessage('integer value for \'perPage\' is required'),
    query('filters.keywords')
      .optional()
      .isString()
      .withMessage('value for \'keywords\' must be string type'),
    query('filters.with_shop')
      .optional().
      isBoolean().
      toBoolean().
      withMessage('value for \'with_shop\' must be boolean type'),
  ],
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const shopId = req.params.shopId;

    const { page, perPage, filters } = req.query;
    const offset = (page - 1) * perPage;

    const include = [
      {
        model: models.ProductImage,
        as: 'images',
      },
      {
        model: models.ProductOption,
        as: 'options'
      }
    ];



    let options = {
      offset,
      limit: perPage,
      where: {
        shopId: shopId
      },
      include
    }

    if (filters) {
      if (filters.with_shop) {
        include.push({
          model: models.Shop,
          as: 'shop',
        });
      }
      if (filters.keywords) {
        options.where[Op.or] = {
          name: {
            [Op.iLike]: filters.keywords + "%"
          }
        }
      }
    }

    const data = await models.Product.findAndCountAll(options);

    data.total_pages = Math.ceil(data.count / perPage)
    data.current_page = page
    data.per_page = perPage

    res.status(200).send({ data });
    res.end();
  });

export default router;
