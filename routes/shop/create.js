import express from 'express'
import models from '../../database/models'
import { query, body, validationResult } from 'express-validator'
import { jwtMiddleware } from '../../middlewares'
import { Op, literal } from 'sequelize'

const router = express.Router()

router.post('/create', [
  jwtMiddleware,
  body('name')
    .isString().isLength({ min: 3, max: 20 }).withMessage('string length should be between 3-20 characters'),
  body('description')
    .isString().withMessage('string value for \'description\' is required'),
  body('imageUrl').isLength({ min: 3, max: 200 })
    .isString().withMessage('string value for \'imageUrl\' is required'),

], async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  let {
    name,
    description,
    imageUrl
  } = req.body

  if (req.user.role === 'superuser') {
    try {

      let userShop = await models.Shop.findOne({
        where: {
          userId: req.user.id
        }
      })

      if (!userShop) {

        await models.Shop.create({
          name,
          description,
          imageUrl,
          userId: req.user.id
        })

        res.status(200).send({ success: true });

      } else {
        return res.status(400).send({ data: "User already has shop" })
      }

    } catch (e) {
      return res.status(500).send({ data: "Server error", error: e })
    }

  } else {
    res.status(403).send({ data: "Shop can be created by superuser" })
  }

  res.end();
});

export default router;
