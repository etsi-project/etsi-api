import express from 'express'
import models from '../../database/models'
import { query, body, validationResult } from 'express-validator'
import { jwtMiddleware } from '../../middlewares'
import { Op, literal } from 'sequelize'
import createApi from './create'
import productsApi from './products'
import paymentApi from './payment'
import router from '../merchant/user'

router.get(
  "/:id",

  async (req, res, next) => {
    const id = req.params.id;

    const include = [
      {
        model: models.User,
        as: "user",
      },
    ];

    const data = await models.Shop.findOne({
      where: {
        id: parseInt(id),
      },
      include,
    });

    res.status(200).send({ data });
    res.end();
  }
);

router.put(
  "/:id/edit", [
  jwtMiddleware,
  body('name')
    .isString(),
  body('description')
    .isString(),
  body('imageUrl')
    .isString()
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let {
    name,
    description,
    imageUrl
  } = req.body

  const id = req.params.id;
  const data = await models.Shop.update(
    {
      name: name,
      description: description,
      imageUrl: imageUrl
    },
    { where: { id: id } }
  )
  if (req.user.role === 'superuser') {
    res.status(200).send({ data });
    res.end();
  }
  else {
    return res.status(400).send({ data: "Your are not superuser for this action" })
  }
}
);

router.delete("/:id", async (req, res, next) => {
  const id = req.params.id;
  const data = await models.Shop.destroy({
    where: {
      id: id,
    },
  }).then(
    function (rowDeleted) {
      if (rowDeleted === 1) {
        console.log("Deleted successfully");
      }
    },
    function (err) {
      console.log(err);
    }
  );
  res.status(200).send({ data });
  res.end();
});

router.use('/', createApi)
router.use('/', productsApi)
router.use('/', paymentApi)

export default router

