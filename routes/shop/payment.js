import express from 'express'
import models from '../../database/models'
import { query, body, validationResult } from 'express-validator'
import { jwtMiddleware } from '../../middlewares'
import { Op, literal } from 'sequelize'

const router = express.Router()

const upsert = (values, condition) => {
  return models.PaymentInfo
    .findOne({ where: condition })
    .then((obj) => {
      if (obj)
        return obj.update(values);
      return models.PaymentInfo.create(values);
    })
}

router.get('/:id/payment/', async (req, res, next) => {
  const shopId = parseInt(req.params.id)

  const data = await models.PaymentInfo.findOne({
    where: {
      shopId
    }
  });

  res.status(200).send({ data });
  res.end();

});

router.put('/:id/payment/', [
  body('iban')
    .isString().isLength({ min: 3, max: 20 }).withMessage('IBAN length should be between 3-20 characters'),
  body('address')
    .isString().withMessage('string value for \'address\' is required')
], async (req, res, next) => {
  const shopId = parseInt(req.params.id)
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  let {
    iban,
    address
  } = req.body

  try {
    const data = await upsert({
      iban,
      address,
      shopId,
      createdAt: Date.now(),
      updatedAt: Date.now()
    }, { shopId })

    res.status(200).send({ data });
  } catch (e) {
    res.status(433).send({ error: "Transaction failed" + e });
  }

});

export default router;
