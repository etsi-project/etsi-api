import express from 'express'
const router = express.Router();
import models from '../../database/models'

router.get('/', async (req, res, next) => {

  const categories = await models.ProductCategory.findAll()

  res.status(200).send(categories)
})

router.get('/:id', async (req, res, next) => {

  const id = req.params.id

  const categories = await models.ProductCategory.findAll({
    where: { id }
  })

  res.status(200).send(categories)
})




export default router