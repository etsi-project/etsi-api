import express from 'express'
import jwt from "jsonwebtoken"
import models from '../../database/models'
import redis from '../../redisConnect'
import { Op, literal } from 'sequelize'
import bcrypt from 'bcrypt'

const router = express.Router()

function getAccessToken(user) {
  return jwt.sign({ user }, process.env.SECRET_KEY, {
    algorithm: "HS256",
    expiresIn: parseInt(process.env.EXPIRY_SECONDS),
  })
}

async function getRefreshToken(user, userId) {
  const refreshToken = jwt.sign({ user }, process.env.SECRET_KEY, { expiresIn: '30d' });
  await redis.set(userId, refreshToken)
  return refreshToken
}

router.post('/token', async (req, res, next) => {
  const form = {
    identifier: req.body.identifier,
    password: req.body.password
  }
  console.log("here",req)

  if (!form.identifier || !form.password) {
    return res.status(401).send({ message: "Username and password is required" }).end()
  }

  const data = await models.User.findOne({
    where: {
      [Op.or]: [
        {
          email: form.identifier
        },
        {
          username: form.identifier
        }
      ]
    }
  })

  if (!data) {
    return res.status(401).send({ message: "User can't be found with given email or username" }).end()
  } else {
    bcrypt.compare(form.password, data.dataValues.encryptedPassword, async (err, result) => {
      if (result) {
        const token = getAccessToken(data)

        const refreshToken = await getRefreshToken(data, data.id)

        const response = {
          refreshToken: refreshToken,
          accessToken: token
        }

        res.status(200).send({
          data: response
        })

        res.end()
      } else {
        return res.status(401).send({ message: "Incorrect password" }).end()
      }
    });
  }
})

router.post('/refresh', async (req, res, next) => {
  const refreshToken = req.body.refreshToken;

  if (!refreshToken) {
    return res.status(403).send({ message: 'Access is forbidden' });
  }

  try {
    const newTokens = await getUpdatedTokens(refreshToken, res);

    res.send(newTokens);
  } catch (err) {
    const message = (err && err.message) || err;
    res.status(403).send(message);
  }
})

async function getUpdatedTokens(token) {
  const decodedToken = jwt.verify(token, process.env.SECRET_KEY);

  const currentToken = await redis.get(decodedToken.user.id)

  if (!currentToken) {
    throw new Error(`Refresh token doesn't exist`);
  }

  const user = await models.User.findOne({ where: { email: decodedToken.user.email } })

  if (!user) {
    throw new Error(`Access is forbidden`);
  }

  const newRefreshToken = await getUpdatedRefreshToken(token, user);
  const newAccessToken = getAccessToken(user);

  return {
    accessToken: newAccessToken,
    refreshToken: newRefreshToken
  };

}

async function getUpdatedRefreshToken(oldRefreshToken, user) {
  const newRefreshToken = jwt.sign({ user }, process.env.SECRET_KEY, { expiresIn: '30d' });
  let dbData = await redis.set(user.id, newRefreshToken)

  return newRefreshToken;
}

export default router