import auth from './auth'
import user from './user'
import create from './create'

export default {
  auth,
  user,
  create
}