import express from 'express'
import jwt from "jsonwebtoken"
import models from '../../database/models'
import redis from '../../redisConnect'
import { query, body, validationResult } from 'express-validator'
import bcrypt from 'bcrypt'

const router = express.Router()

router.post('/', [
  body('email')
    .isEmail()
    .isString().isLength({ min: 3, max: 20 }),
  body('id')
    .isString().withMessage('string value for \'id\' is required'),
  body('shopName')
    .isString().withMessage('string value for \'shopName\' is required'),
  body('juridicalName').optional()
    .isString().withMessage('string value for \'juridicalName\' is required'),
  body('fullname').optional()
    .isString().withMessage('string value for \'juridicalName\' is required'),
  body('contactPerson').optional().isString().withMessage("string value for 'contactPerson' is required "),
  body('phoneNumber').optional().isString().withMessage("string value for 'phoneNumber' is required "),
  body('personType').isString().withMessage("string value for 'personType' is required "),
  body('password').isString().withMessage("string value for 'password' is required ")

], async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  let {
    email,
    id,
    shopName,
    fullname,
    juridicalName,
    contactPerson,
    phoneNumber,
    personType,
    password
  } = req.body

  const t = await models.sequelize.transaction();

  let emailExists = await models.User.findOne({ where: { email } })

  if (emailExists) {
    return res.status(409).send({ message: "User with email " + email + " already exists" })
  }

  try {

    const saltRounds = 10;

    const hash = bcrypt.hashSync(password, saltRounds);

    const user = await models.User.create({
      identificationCode: id,
      role: "merchant",
      encryptedPassword: hash,
      email,
      fullname,
      profileImage: "https://static.etsi.ge/uploads/placeholder.png",
      createdAt: new Date(),
      updatedAt: new Date()
    }, { transaction: t });

    let shop = await models.Shop.create({
      identificationCode: id,
      name: shopName,
      juridicalName,
      contactPerson: contactPerson || fullname,
      phoneNumber,
      personType,
      userId: user.id,
      createdAt: new Date(),
      updatedAt: new Date()
    }, { transaction: t })

    await t.commit();
    res.send({ success: true })

  } catch (error) {
    await t.rollback();
    return res.send({ data: error })
  }
  res.end()
})

export default router