import express from 'express'
import models from '../../database/models'
import jwt from "jsonwebtoken"
import { jwtMiddleware } from '../../middlewares'

const router = express.Router();

router.get('/', jwtMiddleware, async (req, res, next) => {
  try {
    const user = await models.User.findOne({ where: { email: req.user.email } })
    const shop = await models.Shop.findOne({ where: { userId: user.id } })

    return res.status(200).send({ data: { user, shop } })
  } catch (e) {
    return res.status(401).send({ message: "Database transaction failed", error: e }).end()
  }
})

export default router