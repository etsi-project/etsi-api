import express from "express";
import models from "../../database/models";
import { query, validationResult } from "express-validator";
import { jwtMiddleware } from "../../middlewares";

const router = express.Router();

router.get(
  "/",
  [
    query("page").isInt().toInt().withMessage("integer value for 'page' is required"),
    query("perPage")
      .toInt()
      .isInt()
      .withMessage("integer value for 'perPage' is required"),
  ],
  async (req, res, next) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    const id = req.params.id;
    const { page, perPage } = req.query;
    const offset = (page - 1) * perPage;

    const include = [
      {
        model: models.User,
        as: "user",
      },
    ];

    const data = await models.Shop.findAndCountAll({
      offset,
      limit: parseInt(perPage),
      include,
    });

    data.total_pages = Math.ceil(18 / 10)
    data.current_page = page
    data.per_page = perPage

    res.status(200).send({ data });
    res.end();
  }
);

export default router;
