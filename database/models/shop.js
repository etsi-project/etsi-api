"use strict";
module.exports = (sequelize, DataTypes) => {
  const Shop = sequelize.define(
    "Shop",
    {
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      userId: DataTypes.INTEGER,
      imageUrl: DataTypes.STRING,
      juridicalName: DataTypes.STRING,
      identificationCode: DataTypes.STRING,
      contactPerson: DataTypes.STRING,
      phoneNumber: DataTypes.STRING,
      personType: DataTypes.STRING,
      isPaymentVerified: DataTypes.BOOLEAN
    },
    {}
  );

  Shop.associate = function (models) {
    Shop.belongsTo(models.User, { foreignKey: "userId", as: "user" });
  };

  return Shop;
};
