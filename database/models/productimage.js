'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProductImage = sequelize.define('ProductImage', {
    productId: DataTypes.INTEGER,
    imageUrl: DataTypes.STRING
  }, {});
  ProductImage.associate = function (models) {

  };
  return ProductImage;
};