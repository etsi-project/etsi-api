'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    SKU: DataTypes.INTEGER,
    name: DataTypes.STRING,
    discount: DataTypes.INTEGER,
    price: DataTypes.INTEGER,
    weight: DataTypes.INTEGER,
    description: DataTypes.STRING,
    categoryId: DataTypes.INTEGER,
    shopId: DataTypes.INTEGER
  }, {});
  Product.associate = function (models) {
    Product.hasMany(models.ProductOption, { foreignKey: 'productId', as: "options" })
    Product.hasMany(models.ProductImage, { foreignKey: 'productId', as: "images" })
    Product.belongsTo(models.Shop, { foreignKey: 'shopId', as: 'shop' })
    Product.belongsTo(models.ProductCategory, { foreignKey: 'categoryId', as: 'productCategory' })
  };
  return Product;
};