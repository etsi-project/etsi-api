'use strict';
module.exports = (sequelize, DataTypes) => {
  const PaymentInfo = sequelize.define('PaymentInfo', {
    shopId: DataTypes.INTEGER,
    iban: DataTypes.STRING,
    address: DataTypes.STRING
  }, {});
  PaymentInfo.associate = function (models) {
    PaymentInfo.belongsTo(models.Shop, { foreignKey: "shopId", as: "shop" });
  };
  return PaymentInfo;
};