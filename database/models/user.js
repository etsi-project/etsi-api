'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    role: DataTypes.STRING,
    profileImage: DataTypes.STRING,
    fullname: DataTypes.STRING,
    identificationCode: DataTypes.STRING,
    encryptedPassword: DataTypes.STRING
  }, {
  });
  User.associate = function (models) {
  };
  return User;
};