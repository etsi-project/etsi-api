'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProductOption = sequelize.define('ProductOption', {
    productId: DataTypes.INTEGER,
    configName: DataTypes.STRING,
    configValue: DataTypes.STRING,
    configSKU: DataTypes.INTEGER
  }, {});
  ProductOption.associate = function (models) {

  };
  return ProductOption;
};