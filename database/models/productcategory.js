'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProductCategory = sequelize.define('ProductCategory', {
    name: DataTypes.STRING,
  }, {  
    timestamps: false,
  });
  ProductCategory.associate = function (models) {
  };
  return ProductCategory;
};