import express from "express";
import bodyParser from "body-parser";
import path from "path";
import cors from "cors";

import auth from "./routes/auth";
import home from "./routes/home";
import shop from "./routes/shop";
import shops from "./routes/shops";
import products from "./routes/products";
import product from "./routes/product";
import upload from "./routes/upload";
import categories from "./routes/categories";

import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger.json';


const app = express();

import merchantRoutes from "./routes/merchant";
import logger from "morgan";

app.use(cors());

app.use(logger("dev"));
app.use(express.static(path.join(__dirname, "./public")));

app.set('views', path.join(__dirname, 'views'));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.options('*', cors())

app.use(express.json({ extended: false }));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use("/", home);


// AUTH SERVICES
app.use("/auth", auth);


// SHOP MODEL SERVICES
app.use("/shop", shop);

// SHOPS SERVICES
app.use("/shops", shops);


// PRODUCTS SERVICES
app.use("/products", products);


// PRODUCT MODEL SERVICES
app.use("/product", product);

// UPLOAD SERVICES
app.use("/upload", upload);


// MERCHANT SERVICES
app.use("/merchant/user", merchantRoutes.user);
app.use("/merchant/auth", merchantRoutes.auth);
app.use("/merchant/create", merchantRoutes.create);

// CATEGORIES SERVICES
app.use("/categories", categories)

function clientErrorHandler(req, res, next) {
  res.status(404).send("Sorry can't find that!");
}

function errorHandler(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }
  res.status(500);
  res.render("error", { error: err });
}

app.use(errorHandler);
app.use(clientErrorHandler);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log("Listening on port:", PORT);
});
