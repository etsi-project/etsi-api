const jwt = require("jsonwebtoken")

function verifyJWTToken(token) {
  return new Promise((resolve, reject) => {
    if (!token.startsWith('Bearer')) {
      return reject('Token is invalid');
    }
    token = token.slice(7, token.length);

    jwt.verify(token, process.env.SECRET_KEY, (err, decodedToken) => {
      if (err) {
        return reject(err.message);
      }

      if (!decodedToken || !decodedToken.user) {
        return reject('Token is invalid');
      }

      resolve(decodedToken.user);
    })
  })
}

function jwtMiddleware(req, res, next) {
  const token = req.get('Authorization');
  if (!token) {
    return res.status(401).send('Token is invalid');
  }

  verifyJWTToken(token)
    .then(user => {
      req.user = user;
      next();
    }).catch(err => {
      res.status(401).send({ message: "Token verification error", error: err });
    });
}

module.exports = {
  jwtMiddleware,
}