const Redis = require('ioredis')

const MAX_REDIS_CONNECT_RETRY = 5

const redis = new Redis({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  db: process.env.REDIS_DB,
  password: process.env.REDIS_PASSWORD,
  keyPrefix: process.env.REDIS_CACHE_PREFIX,
  connectTimeout: 3000,
  maxRetriesPerRequest: 2,
  retryStrategy: (currentRetry) => {
    return currentRetry < MAX_REDIS_CONNECT_RETRY ? currentRetry : false
  },

})

redis.on('connect', () => {
  console.log("------REDIS: Connected-------")
})

module.exports = redis